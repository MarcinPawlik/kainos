package ut.com.atlassian.kainos.jira;

import org.junit.Test;
import com.atlassian.kainos.jira.MyPluginComponent;
import com.atlassian.kainos.jira.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}