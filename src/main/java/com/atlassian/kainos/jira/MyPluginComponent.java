package com.atlassian.kainos.jira;

public interface MyPluginComponent
{
    String getName();
}